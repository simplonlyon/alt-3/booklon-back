import {Strategy as GoogleStrategy, } from 'passport-google-oauth20';
import passport from 'passport';

passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: 'http://localhost:8000/oauth2/redirect/google',
    scope: [ 'profile' ],
    state: false
},
function verify(accessToken, refreshToken, profile, cb) {
    cb(null, profile);
}));