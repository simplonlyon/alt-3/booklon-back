import express from 'express';
import cors from 'cors';
import { crudController } from './controller/generic-controller';
import morgan from 'morgan';
import { Region } from './entity/Region';
import { roomController } from './controller/room-controller';
import { Fabrique } from './entity/Fabrique';
import { Promo } from './entity/Promo';
import { bookingController } from './controller/booking-controller';
import './auth-config';
import { authRouter } from './authentication';
import { studentController } from './controller/student-controller';

export const server = express();

server.use(morgan('dev'))
server.use(express.json())
server.use(cors());
server.use(express.static('public'));

server.use(authRouter);

server.use('/api/region', crudController(Region, null, ['fabriques']));
server.use('/api/fabrique', crudController(Fabrique, ['region'], ['rooms']));
server.use('/api/promo', crudController(Promo, null, ['fabrique', 'bookings']));
server.use('/api/room', roomController);
server.use('/api/booking', bookingController);
server.use('/api/student', studentController);

