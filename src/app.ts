import 'dotenv-flow/config';
import { createConnection } from 'typeorm';
import { server } from './server';

const port = process.env.PORT || 8000;

createConnection({
    type: 'mysql',
    url: process.env.DATABASE_URL,
    synchronize: true,
    entities: [__dirname +'/entity/*']
});

server.listen(port, () => {
    console.log('listening on ' + port);
});