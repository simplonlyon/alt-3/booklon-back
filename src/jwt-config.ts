import jwt from "jsonwebtoken";
import passport from "passport";
import { ExtractJwt, Strategy } from "passport-jwt";


export function generateToken(payload) {
  const token = jwt.sign(payload, process.env.JWT_SECRET, {
    expiresIn: 60 * 60,
    algorithm: "HS512",
  });
  return token;
}

export function generateRefreshToken(user) {
  return jwt.sign(user, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: "1y",
    algorithm: "HS512",
  });
}

export function configurePassport() {
  passport.use(
    new Strategy(
      {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET,
      },
      async (payload, done) => {
        done(null, true);
      }
    )
  );
}

export function protect(role = ['any']) {

  return [
    passport.authenticate('jwt', { session: false }),
    (req, res, next) => {
      if (role.includes('any') || role.includes(req.user.role)) {
        next()
      } else {
        res.status(403).json({ error: 'Access denied' });
      }
    }
  ]
}