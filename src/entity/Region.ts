import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Fabrique } from "./Fabrique";
import {IsNotEmpty} from 'class-validator'


@Entity()
export class Region {

    @PrimaryGeneratedColumn()
    id:number;
    @Column()
    @IsNotEmpty()
    name:string;
    
    @OneToMany(() => Fabrique, fabrique => fabrique.region)
    fabriques:Fabrique[];
    
}