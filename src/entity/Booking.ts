

import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Promo } from "./Promo";
import { Room } from "./Room";
import {IsNotEmpty, IsDate, IsDateString} from 'class-validator';
@Entity()
export class Booking {

    @PrimaryGeneratedColumn()
    id:number;
    @Column()
    @IsDateString()
    @IsNotEmpty()
    startDate:Date;
    @Column()
    @IsDateString()
    @IsNotEmpty()
    endDate:Date;
    @ManyToOne(() => Room, room => room.bookings)
    @IsNotEmpty()
    room:Room;
    @ManyToOne(() => Promo, promo => promo.bookings, {onDelete: 'CASCADE'})
    @IsNotEmpty()
    promo:Promo;
    
}