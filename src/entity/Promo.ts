

import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Booking } from "./Booking";
import {IsNotEmpty, IsOptional, IsDateString} from 'class-validator'
import { Fabrique } from "./Fabrique";
import { Student } from "./Student";

@Entity()
export class Promo {

    @PrimaryGeneratedColumn()
    id:number;
    @Column()
    @IsNotEmpty()
    name:string;
    @Column()
    @IsDateString()
    @IsOptional()
    startDate:Date;
    @Column()
    @IsDateString()
    @IsOptional()
    endDate:Date;
    @Column()
    @IsOptional()
    studentNumber:number;
    
    @ManyToOne(() => Fabrique, (fabrique) => fabrique.promos)
    fabrique:Fabrique;
    @OneToMany(() => Booking, booking => booking.promo)
    bookings: Booking[];
    @OneToMany(() => Student, student => student.promo)
    students: Student[];
    
}