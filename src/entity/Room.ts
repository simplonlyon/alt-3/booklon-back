

import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Booking } from "./Booking";
import { Fabrique } from "./Fabrique";
import {IsNotEmpty, IsPositive, IsOptional} from 'class-validator'


@Entity()
export class Room {

    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    @IsNotEmpty()
    name: string;
    @Column()
    @IsOptional()
    capacity: number;
    @Column()
    @IsOptional()
    color:string;

    @ManyToOne(() => Fabrique, (fabrique) => fabrique.rooms)
    fabrique: Fabrique;
    @OneToMany(() => Booking, booking => booking.room)
    bookings: Booking[];

}