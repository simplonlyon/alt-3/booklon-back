

import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Promo } from "./Promo";
import { Region } from "./Region";
import { Room } from "./Room";
import {IsNotEmpty} from 'class-validator'


@Entity()
export class Fabrique {

    @PrimaryGeneratedColumn()
    id:number;
    @Column()
    @IsNotEmpty()
    name:string;
    
    @ManyToOne(() => Region, (region) => region.fabriques)
    region:Region;
    @OneToMany(() => Promo, promo => promo.fabrique)
    promos: Promo[];
    @OneToMany(() => Room, room => room.fabrique)
    rooms: Room[];
    
}