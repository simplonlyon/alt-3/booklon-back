import { IsNotEmpty } from "class-validator";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Promo } from "./Promo";

@Entity()
export class Student {

    @PrimaryGeneratedColumn()
    id:number;
    @Column()
    @IsNotEmpty()
    name:string;
    @ManyToOne(() => Promo, promo => promo.students, {onDelete: 'CASCADE'})
    promo:Promo;
}