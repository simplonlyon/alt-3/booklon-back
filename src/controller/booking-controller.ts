import { Between, getRepository, LessThan, LessThanOrEqual, MoreThan, MoreThanOrEqual } from "typeorm";
import { Booking } from "../entity/Booking";
import { crudController } from "./generic-controller";



export const bookingController = crudController(Booking);


bookingController.get('/range/:fabrique/:start/:end', async (req, res) => {
    try {
        const start = new Date(req.params.start);
        const end = new Date(req.params.end);
        if(start.toString() == 'Invalid Date' || end.toString() == 'Invalid Date') {
            res.status(400).json({error: 'Invalid date format'});
            return;
        }
        start.setHours(0);
        end.setHours(23);
        const entities = await getRepository(Booking).find({relations:['promo','room'], where:{promo:{fabrique:req.params.fabrique }, startDate: LessThanOrEqual(end), endDate: MoreThanOrEqual(start)}});

        res.json(entities);

    } catch (error) {
        res.status(500).json(error);
    }
});