import { Router } from "express";
import { EntityColumnNotFound, getRepository } from "typeorm";
import {validate} from 'class-validator';
import { protect } from "../jwt-config";


export function crudController(entityClass:{ new(...args: any[]): any; }, allRelations?:string[], oneRelations?:string[]) {
    const genericController = Router();

    genericController.get('/', async (req, res) => {

        try {
            
            const entities = await getRepository(entityClass).find({relations:allRelations, where: req.query});

            res.json(entities);

        } catch (error) {
            if(error instanceof EntityColumnNotFound) {
                res.status(400).json({message:error.message});
            }
            res.status(500).json(error);
        }
    })




    genericController.post('/', protect(), async (req, res) => {
        try {
            let toSave = [];
            let wasArray = false;
            if(!Array.isArray(req.body)) {
               toSave.push(req.body);
            } else {
                wasArray = true;
                toSave = req.body;
            }
            toSave = toSave.map(item => Object.assign(new entityClass(), item));            
            
            const errors = [];
            for (const item of toSave) {
                errors.push(...await validate(item));
            }
            if(errors.length > 0) {
                res.status(400).json(errors);
                return;
            }

            await getRepository(entityClass).save(toSave);
            if(!wasArray) {
                res.status(201).json(toSave[0]);
                return;
            }
            res.status(201).json(toSave);
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }

    });


    genericController.get('/:id', async (req, res) => {
        try {
            const entity = await getRepository(entityClass).findOne(req.params.id, {relations:oneRelations});
            if (!entity) {
                res.status(404).end();
                return;
            }
            res.json(entity);
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    });

    genericController.patch('/:id', protect(), async (req, res) => {
        try {
            const repo = getRepository(entityClass);
            const entity = await repo.findOne(req.params.id);
            if (!entity) {
                res.status(404).end();
                return;
            }
            Object.assign(entity, req.body);
            const errors = await validate(entity);
            if(errors.length > 0) {
                res.status(400).json(errors);
                return;
            }
            
            repo.save(entity);

            res.json(entity);
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    })


    genericController.delete('/:id', protect(), async (req, res) => {
        try {
            const repo = getRepository(entityClass);
            const result = await repo.delete(req.params.id);
            if (result.affected !== 1) {
                res.status(404).end();
                return;
            }


            res.status(204).end();
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }
    })

    return genericController;
}