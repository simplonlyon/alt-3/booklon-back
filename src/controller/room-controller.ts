import { getRepository } from "typeorm";
import { Room } from "../entity/Room";
import { crudController } from "./generic-controller";


export const roomController = crudController(Room, [], ['bookings', 'bookings.promo']);


roomController.get('/fabrique/:id', async (req,res) => {
    try {
            
        const entities = await getRepository(Room).find({relations:['bookings','bookings.promo'], where:{fabrique:req.params.id}});

        res.json(entities);

    } catch (error) {
        res.status(500).json(error);
    }
})