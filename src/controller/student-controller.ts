import { getRepository } from "typeorm";
import { Student } from "../entity/Student";
import { crudController } from "./generic-controller";



export const studentController = crudController(Student, [], ['promo']);


studentController.get('/randomized/:id', async (req, res) => {
    try {
       const students = await getRepository(Student).createQueryBuilder('user')
       .where('user.promo = :id', {id:req.params.id})
       .orderBy('RAND(WEEK(NOW()))')
       .getMany();
       

        res.json(students);

    } catch (error) {
        res.status(500).json(error);
    }
})