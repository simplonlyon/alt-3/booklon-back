import { Request, Response, Router } from "express";
import passport from "passport";
import { configurePassport, generateRefreshToken, generateToken, protect } from "./jwt-config";
import jwt from "jsonwebtoken";
configurePassport();

export const authRouter = Router();

authRouter.get('/api/login', passport.authenticate('google', { session: false }));

authRouter.get('/oauth2/redirect/google', passport.authenticate('google', { session: false, failureRedirect: '/api/login' }), (req, res) => {
    const token = generateToken({ connected: true });
    const refreshToken = generateRefreshToken({ refresh: true });
    res.send(`
   <html>
    <body>
        <script>
            
            window.opener.postMessage({token:'${token}', refreshToken:'${refreshToken}'}, '*');
        </script>
    </body>
   </html>
   `);
});

authRouter.post('/api/refresh', (req: Request, res: Response) => {
    if (jwt.verify(req.body.token, process.env.REFRESH_TOKEN_SECRET)) {

        res.json({
            token: generateToken({ msg: 'connected' })
        })
        return;
    }
    res.status(403).json({error: 'Access denied'});
})